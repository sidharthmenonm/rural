import jQuery from "jquery";
import slick from "slick-carousel";
import slickAnimation from './slick-animation';
import countdown from './jquery.countdown';

import csvtojson from './csvtojson';
import Vue from 'vue';

jQuery('.slider').slick({
  autoplay: true,
  speed: 1200,
  lazyLoad: 'progressive',
  arrows: false,
  dots: true,
}).slickAnimation();

$('#countdown').countdown({ until: new Date(2024, 12-1, 14) });

$(window).on('load', function() {
  $('#preload').delay(350).fadeOut('slow');
});

$('.challenge-content > a.button').on('click', function(e) {
  e.preventDefault();
  $(this).parent().children('.slide').fadeIn();
});

$('.slide > .close').on('click', function(e) {
  e.preventDefault();
  $(this).parent().fadeOut();
})

$('#accordion').collapse()

new Vue({
  el: '#speaker-section',
  data: {
    data: [],
    speakers: [],
    key: '2PACX-1vTrHhz2JHB3idxn4hlztpbZQMlx7dmFUW24MImFmRJz6EZDhxfztW1_Agq9-XnpCc96Qc0wuAk1yX9i',
    gid: '0'
  },
  computed: {
    // speakers: function() {
    //   return this.sort('speaker')
    // },
    // investors: function() {
    //   return this.sort('curator')
    // }
  },
  methods: {
    // sort: function(typ) {
    //   var sorted = this.data
    //     // .filter(function(item) {
    //     //   return item.type == typ;
    //     // })

    //   // Set slice() to avoid to generate an infinite loop!
    //   sorted = sorted.slice().sort(function(a, b) {
    //     return a.order - b.order;
    //   });

    //   return sorted.filter(function(item) {
    //     return item.order > 0
    //   })
    // },
    // loadData: function() {
    //   var vm = this;
    //   fetch(`https://docs.google.com/spreadsheets/d/e/${this.key}/pub?gid=${this.gid}&single=true&output=csv`)
    //     .then(response => response.text())
    //     .then(csv => csvtojson(csv))
    //     .then(function(json) {
    //       vm.data = JSON.parse(json);
    //     });
    // },
    loadData: function() {
      var vm = this;
      fetch(`https://events.startupmission.in/api/event/rural_conclave_2024/speakers`)
        .then(response => response.json())
        .then(function(json) {
          console.log(json['Speakers'], vm.speakers);
          vm.data = json['Speakers'];
        });
    },

  },
  mounted() {
    this.loadData();
  }
})